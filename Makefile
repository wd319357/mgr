jars="libs/jars/commons-cli-1.3.1.jar:libs/jars/gs-ui-1.3.jar:libs/jars/gs-core-1.3.jar:libs/jars/guava-18.0.jar:libs/jars/antlr-4.5.3-complete.jar"

all:
	mkdir -p bin
	javac src/*/*.java -d bin -cp $(jars)

clean:
	rm -R bin