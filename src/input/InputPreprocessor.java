package input;

import java.util.Map;
import java.io.IOException;
import java.util.Set;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.tree.ParseTree;

import hash.SketchyHashVisitor;

import java.util.IdentityHashMap;
import parser.Java8Lexer;
import parser.Java8Parser;
import utils.CommandFailedException;
import utils.GitCommandExecutor;
import utils.ProgamOptions;

public class InputPreprocessor {
	GitCommandExecutor gce;
	String repoPath;
	int hashingDepth;
	
	public InputPreprocessor(String repoPath, int hashingDepth) {
		this.repoPath = repoPath;
		this.hashingDepth = hashingDepth;
		gce = new GitCommandExecutor(repoPath);
	}
	
	public PreprocessedInput gatherInput() throws IOException, InputError, CommandFailedException {
		String gitOutput = gce.git("log", "--pretty=format:%H", "| head -1");
		String commitHash = gitOutput;
		commitHash = commitHash.replace("\n", "");
		
		// get list of files
		gitOutput = gce.git("ls-tree", "-r --name-only", commitHash);
		String[] files = gitOutput.split("\n");
	
		if (files.length  < 1) {
			throw new InputError("No files running program on an empty repo?");
		}
		
		// putting function into buckets
		Map<ParseTree, String> functionToFile = new IdentityHashMap<ParseTree, String>();
		
		Map<ParseTree, Long> functions = gatherHashes(commitHash, files, functionToFile);
		
		return new PreprocessedInput(functionToFile, functions, functions);
	}
	
	protected Map<ParseTree, Long> gatherHashes(String commit, String[] files, Map<ParseTree, String> functionToFile) throws IOException, CommandFailedException {
		String gitOutput;
		Map<ParseTree, Long> functions = new IdentityHashMap<ParseTree, Long>();
		
		for (String fileName: files) {
			// TODO: Naive method
			if (!fileName.endsWith(".java")) {
				continue;
			}
			
			if (ProgamOptions.printProcessing) {
				System.out.println(fileName);
				System.out.println("parsing " + commit + ": " + fileName);
			}

			SketchyHashVisitor visitor = new SketchyHashVisitor(hashingDepth);
			gitOutput = gce.git("show", commit + ":" + fileName);
			// apply comments filtering regex
			// ((['"])(?:(?!\2|\\).|\\.)*\2)|\/\/[^\n]*|\/\*(?:[^*]|\*(?!\/))*\*\/
			// ((['\"])(?:(?!\\2|\\\\).|\\\\.)*\\2)|\\/\\/[^\\n]*|\\/\\*(?:[^*]|\\*(?!\\/))*\\*\\/
			gitOutput = gitOutput.replaceAll("((['\"])(?:(?!\\2|\\\\).|\\\\.)*\\2)|\\/\\/[^\\n]*|\\/\\*(?:[^*]|\\*(?!\\/))*\\*\\/", "");
			
			try {
				ParseTree tree = parseFile(gitOutput);
				Set<ParseTree> parsedFunctions = extractFunctions(tree);
				
				for (ParseTree pf: parsedFunctions) {
					Map<ParseTree, Long> hashedFunction = visitor.visit(pf);
					functions.putAll(hashedFunction);
					
					// NOTE: can be optimized
					for (ParseTree pt: hashedFunction.keySet()) {
						functionToFile.put(pt, fileName);
					}
				}
			} catch (RecognitionException e) {
				// TODO: is it a proper exception caught here?
				System.err.print("Could not parse file: " + fileName);
			}
		}
		
		return functions;
	}
	
	public Set<ParseTree> extractFunctions(ParseTree tree) {
		GatherFunctionsVisitor gfv = new GatherFunctionsVisitor();
		return gfv.visit(tree);
	}
	
	private ParseTree parseFile(String input) throws IOException {
		Java8Lexer lexer = new Java8Lexer(new ANTLRInputStream(input));
		
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		Java8Parser parser = new Java8Parser(tokens);
	    return  parser.compilationUnit();
	}	
}
