package input;

public class InputError extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String msg;
	InputError(String m) {
		msg = m;
	}
	
	public String toString() {
		return msg;
	}
}
