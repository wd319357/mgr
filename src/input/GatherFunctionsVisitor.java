package input;

import java.util.HashSet;
import java.util.Set;
import org.antlr.v4.runtime.tree.ParseTree;
import parser.Java8BaseVisitor;
import parser.Java8Parser;

public class GatherFunctionsVisitor extends Java8BaseVisitor<Set<ParseTree>> {
	@Override
	public Set<ParseTree> visitMethodDeclaration(Java8Parser.MethodDeclarationContext ctx) { 
	
	Set<ParseTree> result = visitChildren(ctx);
		result.add(ctx);
		return result;
	}
	
	@Override
	protected Set<ParseTree> aggregateResult(Set<ParseTree> aggregate, Set<ParseTree> nextResult) {
		aggregate.addAll(nextResult);
		return aggregate;
	}

	@Override
	protected Set<ParseTree> defaultResult() {
		return new HashSet<ParseTree>();
	}
}
