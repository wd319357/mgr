package input;

import java.util.Map;

import org.antlr.v4.runtime.tree.ParseTree;

public class PreprocessedInput {
	Map<ParseTree, String> functionToFile;
	Map<ParseTree, Long> baseFunctions;
	Map<ParseTree, Long> comparedAgainstFunctions;
	
	public PreprocessedInput(Map<ParseTree, String> functionToFile, Map<ParseTree, Long> baseFunctions, Map<ParseTree, Long> comparedAgainstFunctions) {
		this.functionToFile = functionToFile;
		this.baseFunctions = baseFunctions;
		this.comparedAgainstFunctions = comparedAgainstFunctions;
	}
	
	public Map<ParseTree, String> getFunctionToFile() {
		return functionToFile;
	}

	public void setFunctionToFile(Map<ParseTree, String> functionToFile) {
		this.functionToFile = functionToFile;
	}

	public Map<ParseTree, Long> getBaseFunctions() {
		return baseFunctions;
	}

	public void setBaseFunctions(Map<ParseTree, Long> baseFunctions) {
		this.baseFunctions = baseFunctions;
	}

	public Map<ParseTree, Long> getComparedAgainstFunctions() {
		return comparedAgainstFunctions;
	}

	public void setComparedAgainstFunctions(Map<ParseTree, Long> comparedAgainstFunctions) {
		this.comparedAgainstFunctions = comparedAgainstFunctions;
	}
}
