package input;

import java.io.IOException;
import java.util.IdentityHashMap;
import java.util.Map;

import org.antlr.v4.runtime.tree.ParseTree;

import utils.CommandFailedException;

public class IncrementalInputPreprocessor extends InputPreprocessor {
	String oldCommit;
	String newCommit;
	
	public IncrementalInputPreprocessor(String repoPath, int hashingDepth) {
		super(repoPath, hashingDepth);
	}
	
	public IncrementalInputPreprocessor(String repoPath, String oldHash, String newHash, int hashingDepth) {
		super(repoPath, hashingDepth);
		this.oldCommit = oldHash;
		this.newCommit = newHash;
	}

	
	@Override
	public PreprocessedInput gatherInput() throws IOException, InputError, CommandFailedException {
		String gitOutput;
		if (oldCommit == null || newCommit == null) {
			gitOutput = gce.git("log", "--pretty=format:%H", "| head -2");
		
			String[] commitHashes = gitOutput.split("\n");
			if (commitHashes.length != 2) {
				throw new InputError("Repo must contain at least 2 comits.");
			}
			newCommit = commitHashes[0];
			oldCommit = commitHashes[1];
		}
	
		// get list of files
		gitOutput = gce.git("ls-tree", "-r --name-only", oldCommit);
		String[] oldFiles = gitOutput.split("\n");
	
		if (oldFiles.length  < 1) {
			throw new InputError("No files 2 commits behind. Comparing changes on empty repo?");
		}
		
		gitOutput = gce.git("show", "--pretty=format:", "--name-only", newCommit);
		String[] modifiedFiles = gitOutput.split("\n");
		
		if (modifiedFiles.length  < 1) {
			throw new InputError("No changes detected.");
		}
		
		// putting function into buckets
		Map<ParseTree, String> functionToFile = new IdentityHashMap<ParseTree, String>();
		Map<ParseTree, Long> oldFunctions = gatherHashes(oldCommit, oldFiles, functionToFile);
		Map<ParseTree, Long> modifiedFunctions = gatherHashes(newCommit, modifiedFiles, functionToFile);
		
		return new PreprocessedInput(functionToFile, oldFunctions, modifiedFunctions);
	}
}
