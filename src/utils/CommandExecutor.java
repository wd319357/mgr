package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CommandExecutor {
	public static String executeCommand(String command) throws CommandFailedException {
		String[] script = {
				"/bin/bash",
				"-c",
				command
		};
		
		StringBuffer output = new StringBuffer();
		Process p;
		try {
			p = Runtime.getRuntime().exec(script);
			//System.out.println("Executing: " + command);
			BufferedReader reader = 
					new BufferedReader(new InputStreamReader(p.getInputStream()));

			String line = "";			
			while ((line = reader.readLine())!= null) {
				output.append(line + "\n");
			}
			
			if (p.waitFor() != 0) {
				throw new CommandFailedException("command failed with code " + p.exitValue() +": " + command + "\n\n" + output.toString());
			}
		} catch (InterruptedException e) {
			throw new CommandFailedException("Interrupted");
		} catch (IOException e) {
			throw new CommandFailedException(e.getMessage());
		}
		return output.toString();
	}
	
	public static String executeCommands(String[] command) {
		StringBuffer output = new StringBuffer();
		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader = 
					new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line = "";			
			while ((line = reader.readLine())!= null) {
				output.append(line + "\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return output.toString();
	}
}
