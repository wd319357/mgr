package utils;

public class GitCommandExecutor extends CommandExecutor {
	String repoPath; 
	String commandPrefix;
	public GitCommandExecutor(String path) {
		repoPath = path;
		commandPrefix = "git --git-dir=" + path + "/.git/";
	}
	
	public String git(String command, String... args) throws CommandFailedException {
		String result = commandPrefix + " " + command;
		
		for (String arg : args) {
			result += " " + arg;
		}
		
		return executeCommand(result);
		
	}
}
