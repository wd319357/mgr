package clonemerge;


public class ClonePair {
	double similarity;
	CodePiece first;
	CodePiece second;

	public ClonePair(CodePiece first, CodePiece second,	double similarity) {
		this.first = first;
		this.second = second;
		this.similarity = similarity;
	}

	public double getSimilarity() {
		return similarity;
	}

	public void setSimilarity(double similarity) {
		this.similarity = similarity;
	}

	public CodePiece getFirst() {
		return first;
	}

	public void setFirst(CodePiece first) {
		this.first = first;
	}

	public CodePiece getSecond() {
		return second;
	}

	public void setSecond(CodePiece second) {
		this.second = second;
	}
}
