package hash;

import java.util.Map;
import java.util.Vector;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.RuleNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import parser.Java8BaseVisitor;
import java.util.IdentityHashMap;

public abstract class HashVisitor extends Java8BaseVisitor<Map<ParseTree, Long>> {
	
	@Override
	protected Map<ParseTree, Long> defaultResult() {
		return new IdentityHashMap<ParseTree, Long>();
	}
	
	protected Map<ParseTree, Long> defaultResult(ParseTree pt, Long h) {
		Map<ParseTree, Long> result = new IdentityHashMap<ParseTree, Long>();
		result.put(pt, h);
		return result;
	}
	
	@Override
	public Map<ParseTree, Long> visitChildren(RuleNode node) {
		Map<ParseTree, Long> result = defaultResult();
		int n = node.getChildCount();
		Vector<Long> hashes = new Vector<Long>();
		
		for (int i = 0; i < n; i++) {
			if (!shouldVisitNextChild(node, result)) {
				break;
			}
			
			ParseTree c = node.getChild(i);
			Map<ParseTree, Long> childResult = c.accept(this);
			hashes.add(childResult.get(c));
			result = aggregateResult(result, childResult);
		}
		
		if (n == 1) {
			result.remove(node.getChild(0));
		}
		
		Long hash = hashFunction(hashes);
		result.put(node, hash);
		
		return result;
	}
	
	
	abstract protected Long hashFunction(Vector<Long> hashes);

	@Override
	protected Map<ParseTree, Long> aggregateResult(Map<ParseTree, Long> aggregate, Map<ParseTree, Long> nextResult) {
		aggregate.putAll(nextResult);
		return aggregate;
	}

	@Override
	public Map<ParseTree, Long> visitTerminal(TerminalNode node) {
		//node.getSymbol().
		Long type = new Long(node.getSymbol().getType());
		return defaultResult(node, type);
	}
}
