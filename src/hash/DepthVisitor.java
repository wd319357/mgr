package hash;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.RuleNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import parser.Java8BaseVisitor;
import parser.Java8Parser;

public class DepthVisitor extends Java8BaseVisitor<Integer> {

	@Override
	protected Integer aggregateResult(Integer aggregate, Integer nextResult) {
		return Math.max(aggregate, nextResult + 1);
	}
	
	@Override
	public Integer visitChildren(RuleNode node) {
		Integer result = defaultResult();
		Integer childResult = defaultResult();
		int n = node.getChildCount();
		
		for (int i=0; i<n; i++) {
			if (!shouldVisitNextChild(node, result)) {
				break;
			}

			ParseTree c = node.getChild(i);
			childResult = c.accept(this);
			result = aggregateResult(result, childResult);
		}

		if (n == 1) { 
			return childResult;
		} else {
			return result;
		}
	}
	
	
	@Override public Integer visitUnaryExpression(Java8Parser.UnaryExpressionContext ctx) { 
		Integer result = visitChildren(ctx);
		everyVisit(ctx, result); 
		return result + 1;
	}

	@Override
	public Integer visitTerminal(TerminalNode node) {
		return 1;
	}
	
	@Override
	protected Integer defaultResult() {
		return 1;
	}
	
}
