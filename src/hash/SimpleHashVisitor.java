package hash;

import java.util.Vector;

public class SimpleHashVisitor extends HashVisitor {
	static final long prime = 1000000007;
	
	@Override
	protected Long hashFunction(Vector<Long> hashes) {
		Long result = 0L;
		for (int i = 0; i < hashes.size(); i++) {
			result *= prime;
			result += hashes.get(i);
		}
		
		// NOTICE: code above has only esthetic value
		if (result < 0) {
			result += Long.MAX_VALUE;
		}
		return result;
	}
}
