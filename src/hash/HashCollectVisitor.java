package hash;

import java.util.Map;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;

import parser.Java8BaseVisitor;

//TODO: does it even workproperly?
public class HashCollectVisitor extends Java8BaseVisitor<Multiset<Long>> {
	Map<ParseTree, Long> hashes;

	public HashCollectVisitor(Map<ParseTree, Long> hashes) {
		this.hashes = hashes;
	}
	
	@Override
	public void everyVisit(ParserRuleContext ctx, Multiset<Long> result) {
		result.add(hashes.get(ctx));
	}

	@Override
	protected Multiset<Long> defaultResult() {
		return HashMultiset.create();
	}

	
	@Override
	protected Multiset<Long> aggregateResult(Multiset<Long> aggregate, Multiset<Long> nextResult) {
		aggregate.addAll(nextResult);
		return aggregate;
	}
	
}