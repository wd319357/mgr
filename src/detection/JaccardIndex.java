package detection;

import java.util.Map;

import org.antlr.v4.runtime.tree.ParseTree;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;

public class JaccardIndex extends ComponentSetMetrics {
	public JaccardIndex(Map<ParseTree, Long> hashes) {
		super(hashes);
	}
	
	@Override
	protected double collectionDistance(Multiset<Long> a, Multiset<Long> b) {
		Multiset<Long> sum = HashMultiset.create();
		Multiset<Long> intersection = HashMultiset.create();
		
		int aCount, bCount;
		
		for (Long h: a) {
			aCount = a.count(h);
			bCount = b.count(h);
			if (!intersection.contains(h)) {
				intersection.add(h, Math.min(aCount, bCount));
			}
			
			if (!sum.contains(h)) {
				sum.add(h, Math.max(aCount, bCount));
			}
		}
		
		double res = intersection.size();
		return res / sum.size();
	}
}
