package detection;

import java.util.Map;

import org.antlr.v4.runtime.tree.ParseTree;

import com.google.common.collect.Multiset;

import hash.HashCollectVisitor;

public abstract class ComponentSetMetrics extends CloneMetrics {
	Map<ParseTree, Long> hashes;
	
	public ComponentSetMetrics(Map<ParseTree, Long> hashes) {
		this.hashes = hashes;
	}
	
	@Override
	public double distance(ParseTree a, ParseTree b) {
		HashCollectVisitor visitor = new HashCollectVisitor(hashes);
		Multiset<Long> aHashes = visitor.visit(a);
		Multiset<Long> bHashes = visitor.visit(b);
		
		return collectionDistance(aHashes, bHashes);
	}
	
	protected abstract double collectionDistance(Multiset<Long> a, Multiset<Long> b);
}