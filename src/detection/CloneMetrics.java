package detection;

import org.antlr.v4.runtime.tree.ParseTree;

public abstract class CloneMetrics implements Metrics<ParseTree>{
	public abstract double distance(ParseTree a, ParseTree b); 
}
