package main;

import java.io.IOException;
import java.util.Set;

import org.antlr.v4.runtime.ParserRuleContext;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import clonemerge.ClonePair;
import detection.CloneDetector;
import detection.JaccardIndex;
import graph.GraphDrawer;

import input.IncrementalInputPreprocessor;
import input.InputError;
import input.InputPreprocessor;
import input.PreprocessedInput;
import utils.CommandFailedException;
import utils.ProgamOptions;
import utils.ClonePrinter;


public class Main {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Options options = new Options();
		
		options.addOption(Option.builder("p")
				.longOpt("path")
				.required(true)
				.hasArg(true)
				.argName("repoPath")
				.desc("path to git repo")
				.build()
				);
		
		options.addOption(Option.builder("i")
				.longOpt("incremental")
				.required(false)
				.optionalArg(true)
				.argName("commitHash")
				.desc("switch to incremental mode")
				.numberOfArgs(2)
				.build()
				);

		options.addOption(Option.builder("t")
				.longOpt("threshold")
				.required(false)
				.hasArg(true)
				.argName("threshold")
				.desc("clone detection sensitivity threshold(default=0.9)")
				.build()
				);
		
		options.addOption(Option.builder("g")
				.longOpt("graph")
				.required(false)
				.desc("specifies whether to produce visualisation graph or not")
				.build()
				);
		
		options.addOption(Option.builder("l")
				.longOpt("labels")
				.required(false)
				.desc("specifies whether to label vertices in the graph")
				.build()
				);
		
		options.addOption(Option.builder("v")
				.longOpt("verbose")
				.required(false)
				.desc("specifies whether to produce detailed output about every clone pair or not")
				.build()
				);
		
		options.addOption(Option.builder("h")
				.longOpt("help")
				.required(false)
				.desc("shows help")
				.build()
				);
		
		options.addOption(Option.builder("s")
				.longOpt("size")
				.required(false)
				.hasArg(true)
				.desc("minimal clone size in lines")
				.build()
				);

		options.addOption(Option.builder("r")
				.longOpt("raw")
				.required(false)
				.argName("raw")
				.desc("specifies whether to produce raw detailed output, suitable to be parsed by another app")
				.build()
				);
		
		options.addOption(Option.builder("d")
				.longOpt("depth")
				.required(false)
				.hasArg(true)
				.desc("hashing depth")
				.build()
				);
		
		CommandLineParser parser = new DefaultParser();
	    try {
	        // parse the command line arguments
	        CommandLine line = parser.parse(options, args);
	        String repoPath = line.getOptionValue("path");
	        double threshold = 0.9;
	        boolean verbose = false;
	        boolean generateGraph = false;
	        boolean labels = false;
	        boolean raw = false;
	        int minimalSize = 5;
	        int hashingDepth = 4;
	        
	        if (line.hasOption("labels"))
	        	labels = true;
	        if (line.hasOption("raw")) {
	        	raw = true;
	        	ProgamOptions.printProcessing = false;
	        }
	        if (line.hasOption("graph"))
	        	generateGraph = true;
	        if (line.hasOption("verbose"))
	        	verbose = true;
	        if (line.hasOption("threshold"))
	        	threshold = Double.parseDouble(line.getOptionValue("threshold")) ;
	        if (line.hasOption("size"))
	        	minimalSize = Integer.decode(line.getOptionValue("size"));
	        if (line.hasOption("depth"))
	        	hashingDepth = Integer.decode(line.getOptionValue("depth"));
	        
	        if (line.hasOption("help")) {
	        	HelpFormatter formatter = new HelpFormatter();
		        formatter.printHelp("cloneDetector", options, true);
		        return;
	        }
	        
	        InputPreprocessor ip;
	        
	        if (line.hasOption("incremental")) {
	        	String[] commitHashes = line.getOptionValues("incremental");
	        	if (commitHashes != null && commitHashes.length == 2) {
	        		ip = new IncrementalInputPreprocessor(repoPath, commitHashes[0], commitHashes[1], hashingDepth);
	        	} else {
	        		ip = new IncrementalInputPreprocessor(repoPath, hashingDepth);
	        	}
	        } else {
	        	ip = new InputPreprocessor(repoPath, hashingDepth);
	        }
	        
	        PreprocessedInput ppi = ip.gatherInput();
	        
	        if (ProgamOptions.printProcessing) {
				System.out.println("Detecting...");
			}
			
	        CloneDetector detector = new CloneDetector(threshold, minimalSize, new JaccardIndex(ppi.getBaseFunctions()));
			detector.feed(ppi.getFunctionToFile(), ppi.getBaseFunctions(), ppi.getComparedAgainstFunctions());
			Set<ClonePair> clones = detector.detect();

			if (raw) {
				System.out.println(clones.size());
			} else {
				System.out.println("Clones found: "  + clones.size());
			}
			for (ClonePair cl : clones) {
				ParserRuleContext pcrFirst = (ParserRuleContext) cl.getFirst().getParseTreeNode();
				ParserRuleContext pcrSecond = (ParserRuleContext) cl.getSecond().getParseTreeNode();
				
				if (raw) {
					//System.out.println("--\n");
					ClonePrinter.printClonePair(cl, repoPath);
				} else if (verbose) {
					System.out.println("--\n");
					System.out.println("Similarity: " + Math.round(100 * cl.getSimilarity())/100.0);
					System.out.println(cl.getFirst().getFile() + ": " + pcrFirst.start.getLine() + " - " + pcrFirst.stop.getLine());
					System.out.println(cl.getSecond().getFile() + ": " + pcrSecond.start.getLine() + " - " + pcrSecond.stop.getLine());
				}
			}

			if (generateGraph) {
				GraphDrawer gd = new GraphDrawer(clones, labels);
				gd.draw();
			}
	    } catch(ParseException exp) {
	        // oops, something went wrong
	        System.err.println( "Parsing failed.  Reason: " + exp.getMessage() + "\n");
	        // automatically generate the help statement
	        HelpFormatter formatter = new HelpFormatter();
	        formatter.printHelp("cloneDetector", options, true);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InputError e) {
			System.out.println(e.getMessage());
		} catch (CommandFailedException e) {
			e.printStackTrace();
			//System.out.println(e.getMessage());
			System.out.println("please ensure that you've passed proper repo path");
		}
	}
}
